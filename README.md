Задание: https://hub.docker.com/repository/docker/qapropeller/qa-battle/general
1) Спулить контейнер docker pull qapropeller/qa-battle:latest
2) Запустить контенейр docker run -d -p 8080:8080 qapropeller/qa-battle:latest

Запуск всех тестов из терминала mvn clean test
Тесты сгруппированы на положительные и падающие по ошибке
Запуск тестов по группам  mvn clean test -Dgroups=pass
Запуск тестов по группам  mvn clean test -Dgroups=fail

Отчетность можно посмотреть после прохождения всех тестов по команде
mvn allure:serve
в pom.xml добавлены dependency, plugin, reporting/plugin



Selenoid:
https://aerokube.com/cm/latest/
Установка
curl -s https://aerokube.com/cm/bash | bash \
&& ./cm selenoid start --vnc
Чтобы запускать тесты в selenoid нужно узнать ip тестового сайта:
1) зайти в контейнер тестового сайта sudo docker exec -i -t *iddocker* /bin/sh
2) посмотреть ip по команде ip a (получили 172.17.0.4)
этот адрес будет использоваться в тестах siteQaPropeller = "172.17.0.4/";



Jenkins
Устновка https://jenkins.io/doc/book/installing/#debianubuntu
Буду запускать jenkins на порту 9090
    для этого в файле /etc/default/jenkins поменять HTTP_PORT=8080 на HTTP_PORT=9090 (JENKINS_USER=root)
login/password alex/123
sudo service jenkins start/status/stop
