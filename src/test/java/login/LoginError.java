package login;

import org.openqa.selenium.WebDriver;

class LoginError {
    private WebDriver driver;

    LoginError(WebDriver driver) {
        this.driver = driver;
    }

    String getUrl() {
        return driver.getCurrentUrl();
    }

}
