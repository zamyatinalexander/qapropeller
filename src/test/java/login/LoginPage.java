package login;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;

    LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    void enterLogin(String s) {
        WebElement startInputLogin = driver.findElement(By.xpath("//div[@class='card-body']//div[1]"));
        startInputLogin.click();
        WebElement email = driver.findElement(By.id("loginInput"));
        email.click();
        email.sendKeys(s);
    }

    void enterPassword(String s) {
        WebElement startInputPassword = driver.findElement(By.xpath("//div[@class='card-body']//div[2]"));
        startInputPassword.click();
        WebElement password = driver.findElement(By.id("passwordInput"));
        password.sendKeys(s);
    }

    void hoverOverButton() {
        WebElement element = driver.findElement(By.xpath("//button[contains(text(),'Hover me faster!')]"));
        Actions action = new Actions(driver);
        action.moveToElement(element).build().perform();
    }

    void clickSignIn() {
        WebElement signIn = driver.findElement(By.xpath("//div[contains(@class,'card-footer bg-transparent')]//div//img"));
        signIn.click();
    }

    void accept() {
        Alert alert = (new WebDriverWait(driver, 10)).until(ExpectedConditions.alertIsPresent());
        alert.accept();
    }

    void dismiss() {
        Alert alert = (new WebDriverWait(driver, 10)).until(ExpectedConditions.alertIsPresent());
        alert.dismiss();
    }

    String welcomText() {
        WebElement element = driver.findElement(By.xpath("//h4[contains(text(),'Welcome to Propeller Championship!')]"));
        return element.getText();
    }

    LoginError loginError() {
        return new LoginError(driver);
    }

    public LoginPass loginPass() {
        return new LoginPass(driver);
    }
}
