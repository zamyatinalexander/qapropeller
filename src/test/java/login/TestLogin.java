package login;

import base.Base;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestLogin extends Base {

    @Test(description = "Успешная авторизация с корректной парой логин/пароль", groups = "pass")
    public void testLoginPass() {
        openSite(siteQaPropeller);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterLogin("test");
        loginPage.enterPassword("test");
        loginPage.hoverOverButton();
        loginPage.clickSignIn();
        loginPage.accept();
        loginPage.accept();
        LoginPass pass = loginPage.loginPass();
        //Assert.assertTrue(pass.getUrl().contains("http://localhost:8080/main.html"),"Не была открыта страница с информацией о клиентах");
        Assert.assertTrue(pass.getTextArticlesToRead().contains("Articles to read"), "Не была открыта страница с информацией о клиентах");
    }

    //После введение не верных авторизационных данных должна открыться страница с ошибкой
    @Test(description = "Неверный пароль", groups = "fail")
    public void invalidPassword() {
        openSite(siteQaPropeller);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterLogin("test");
        loginPage.enterPassword("test1");
        loginPage.hoverOverButton();
        loginPage.clickSignIn();
        loginPage.accept();
        loginPage.accept();
        LoginError error = loginPage.loginError();
        Assert.assertTrue(error.getUrl().contains("http://localhost:8080/loginError.html"), "Не была открыта страница с ошибкой");
    }

    //После введение не верных авторизационных данных должна открыться страница с ошибкой
    @Test(description = "Неверный логин", groups = "fail")
    public void invalidLogin() {
        openSite(siteQaPropeller);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterLogin("123");
        loginPage.enterPassword("test");
        loginPage.hoverOverButton();
        loginPage.clickSignIn();
        loginPage.accept();
        loginPage.accept();
        LoginError error = loginPage.loginError();
        Assert.assertTrue(error.getUrl().contains("http://localhost:8080/loginError.html"), "Не была открыта страница с ошибкой");
    }

    @Test(description = "Are you sure you want to login? - отклонить действие. Возврат на страницу авторизации", groups = "pass")
    public void confirmDismiss() {
        openSite(siteQaPropeller);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterLogin("test");
        loginPage.enterPassword("test");
        loginPage.hoverOverButton();
        loginPage.clickSignIn();
        loginPage.dismiss();
        Assert.assertTrue(loginPage.welcomText().contains("Welcome to Propeller Championship!"));
    }

    //Если во втором диалоговом окне нажать отмену должен происходить возврат на страницу авторизации. Мое мнение
    @Test(description = "Really sure? - отклонить действие. Возврат на страницу авторизации", groups = "fail")
    public void confirmAcceptDismiss() {
        openSite(siteQaPropeller);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterLogin("test");
        loginPage.enterPassword("test");
        loginPage.hoverOverButton();
        loginPage.clickSignIn();
        loginPage.accept();
        loginPage.dismiss();
        Assert.assertTrue(loginPage.welcomText().contains("Welcome to Propeller Championship!"));
    }
}
