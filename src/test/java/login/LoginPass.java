package login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPass {

    private WebDriver driver;

    LoginPass(WebDriver driver) {
        this.driver = driver;
    }

    public String getTextArticlesToRead() {
        WebElement element = driver.findElement(By.xpath("//div[contains(text(),'Articles to read')]"));
        return element.getText();
    }

    String getUrl() {
        return driver.getCurrentUrl();
    }
}
