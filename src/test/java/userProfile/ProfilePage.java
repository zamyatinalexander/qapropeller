package userProfile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class ProfilePage {
    private WebDriver driver;

    ProfilePage(WebDriver driver) {
        this.driver = driver;
    }

    void openProfile() {
        WebElement element = driver.findElement(By.id("avatar"));
        element.click();
    }

    void setFirstName(String s) {
        WebElement element = driver.findElement(By.id("firstNameInput"));
        element.sendKeys(s);
    }

    void setLastName(String s) {
        WebElement element = driver.findElement(By.id("lastNameInput"));
        element.sendKeys(s);
    }

    String getFirstName() {
        return driver.findElement(By.id("firstNameInput")).getAttribute("value");
    }

    String getLastName() {
        return driver.findElement(By.id("lastNameInput")).getAttribute("value");
    }

    Boolean visibleElementSuccessUserInfoSaveInfo() {
        WebElement element = driver.findElement(By.xpath("//div[@id='successUserInfoSaveInfo']"));
        return element.isDisplayed();
    }

    void saveUserInfo() {
        WebElement element = driver.findElement(By.xpath("//button[contains(text(),'Save user info')]"));
        element.click();
    }

    void refreshPage() {
        driver.navigate().refresh();
    }

    String messagePleaseSetYourFirstName() {
        return driver.findElement(By.xpath("//div[contains(text(),'Please set your first name.')]")).getText();
    }

    String messagePleaseSetYourLastName() {
        return driver.findElement(By.xpath("//div[contains(text(),'Please set your last name.')]")).getText();
    }

    void openPaymentInfo() {
        driver.findElement(By.id("v-pills-messages-tab")).click();
    }

    void setCardNumber(String s) {
        WebElement element = driver.findElement(By.id("cardNumberInput"));
        element.sendKeys(s);
    }

    void choosePaymentSystem(String s) {
        driver.findElement(By.xpath("//option[contains(text(),'" + s + "')]")).click();
    }

    void clickSavePaymentInfo() {
        driver.findElement(By.xpath("//button[contains(text(),'Save payment info')]")).click();
    }

    void selectDayOfPayment() {
        driver.findElement(By.id("paymentRange")).click();
    }

    String getCardNumber() {
        return driver.findElement(By.id("cardNumberInput")).getAttribute("value");
    }

    String getPaymentSystem(String s) {
        WebElement element = driver.findElement(By.xpath("//option[contains(text(),'" + s + "')]"));
        return element.getAttribute("value");
    }

    String messagePleaseSetYourCardNumber() {
        return driver.findElement(By.xpath("//div[contains(text(),'Please set your card number.')]")).getText();
    }

    String messagePleaseSetYourPaymentSystem() {
        return driver.findElement(By.xpath("//div[contains(text(),'Please select your payment system.')]")).getText();
    }
}
