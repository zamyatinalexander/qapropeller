package userProfile;

import base.Base;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestProfile extends Base {

    @Test(description = "Сохранение firstName lastName и проверка, что после обновления страницы данные сохранились", groups = "pass")
    public void saveUserInfoPass() {
        String firstName = "Ivan";
        String lastName = "Ivanov";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.setFirstName(firstName);
        profilePage.setLastName(lastName);
        profilePage.saveUserInfo();
        Assert.assertTrue(profilePage.visibleElementSuccessUserInfoSaveInfo());    //Проверка видимости элемента (сообщения "User info successfully saved")
        profilePage.refreshPage();
        Assert.assertTrue(profilePage.getFirstName().contains(firstName), "First name не сохранился");
        Assert.assertTrue(profilePage.getLastName().contains(lastName), "Last name не сохранился");
    }

    @Test(description = "Сохранение с незаполненным firstName", groups = "pass")
    public void saveUserInfoFirstNameIsEmpty() {
        String firstName = "";
        String lastName = "Ivanov";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.setFirstName(firstName);
        profilePage.setLastName(lastName);
        profilePage.saveUserInfo();
        Assert.assertTrue(profilePage.messagePleaseSetYourFirstName().contains("Please set your first name."));
    }

    @Test(description = "Сохранение с незаполненным lastName", groups = "pass")
    public void saveUserInfoLastNameIsEmpty() {
        String firstName = "Ivan";
        String lastName = "";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.setFirstName(firstName);
        profilePage.setLastName(lastName);
        profilePage.saveUserInfo();
        Assert.assertTrue(profilePage.messagePleaseSetYourLastName().contains("Please set your last name."));
    }

    @Test(description = "Сохранение с заполненным пробелом в firstName", groups = "fail")
    public void saveUserInfoFirstNameIsSpace() {
        String firstName = " ";
        String lastName = "Ivanov";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.setFirstName(firstName);
        profilePage.setLastName(lastName);
        profilePage.saveUserInfo();
        Assert.assertTrue(profilePage.messagePleaseSetYourFirstName().contains("Please set your first name."));
    }

    @Test(description = "Сохранение с заполненным пробелом в lastName", groups = "fail")
    public void saveUserInfoLastNameIsSpase() {
        String firstName = "Ivan";
        String lastName = " ";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.setFirstName(firstName);
        profilePage.setLastName(lastName);
        profilePage.saveUserInfo();
        Assert.assertTrue(profilePage.messagePleaseSetYourLastName().contains("Please set your last name."));
    }

    @Test(description = "Сохранение payment info c платежной системой Visa", groups = "pass")
    public void savePaymentInfoVisa() {
        String cardNumber = "4000000000000002";
        String paymentSystem = "Visa";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.openPaymentInfo();
        profilePage.setCardNumber(cardNumber);
        profilePage.choosePaymentSystem(paymentSystem);
        profilePage.selectDayOfPayment();
        profilePage.clickSavePaymentInfo();
        profilePage.refreshPage();
        profilePage.openPaymentInfo();
        Assert.assertTrue(profilePage.getCardNumber().contains(cardNumber), "Номер карты не сохранился");
        Assert.assertTrue(profilePage.getPaymentSystem(paymentSystem).contains(paymentSystem), "Платежная система не сохранилась");
    }

    @Test(description = "Сохранение payment info c платежной системой MasterCard", groups = "pass")
    public void savePaymentInfoMasterCard() {
        String cardNumber = "5000000000000001";
        String paymentSystem = "MasterCard";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.openPaymentInfo();
        profilePage.setCardNumber(cardNumber);
        profilePage.choosePaymentSystem(paymentSystem);
        //profilePage.choosePaymentSystem("Apple Card");
        profilePage.selectDayOfPayment();
        profilePage.clickSavePaymentInfo();
        profilePage.refreshPage();
        profilePage.openPaymentInfo();
        Assert.assertTrue(profilePage.getCardNumber().contains(cardNumber), "Номер карты не сохранился");
        Assert.assertTrue(profilePage.getPaymentSystem(paymentSystem).contains(paymentSystem), "Платежная система не сохранилась");
    }

    @Test(description = "Сохранение payment info c платежной системой Apple Card", groups = "pass")
    public void savePaymentInfoAppleCard() {
        String cardNumber = "4000000000000002";
        String paymentSystem = "Apple Card";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.openPaymentInfo();
        profilePage.setCardNumber(cardNumber);
        profilePage.choosePaymentSystem(paymentSystem);
        profilePage.selectDayOfPayment();
        profilePage.clickSavePaymentInfo();
        profilePage.refreshPage();
        profilePage.openPaymentInfo();
        Assert.assertTrue(profilePage.getCardNumber().contains(cardNumber), "Номер карты не сохранился");
        Assert.assertTrue(profilePage.getPaymentSystem(paymentSystem).contains(paymentSystem), "Платежная система не сохранилась");
    }

    @Test(description = "Сохранение payment info без заполнения номера карты", groups = "pass")
    public void savePaymentInfoNotCardNumber() {
        String cardNumber = "";
        String paymentSystem = "Visa";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.openPaymentInfo();
        profilePage.setCardNumber(cardNumber);
        profilePage.choosePaymentSystem(paymentSystem);
        profilePage.selectDayOfPayment();
        profilePage.clickSavePaymentInfo();
        Assert.assertTrue(profilePage.messagePleaseSetYourCardNumber().contains("Please set your card number."), "Сообщение о не заполненном поле номер карты не отобразилось");

    }

    @Test(description = "Сохранение payment info без заполнения номера карты", groups = "pass")
    public void savePaymentInfoNotChoosPaymentSystem() {
        String cardNumber = "4000000000000002";
        String paymentSystem = "";
        openSite(siteQaPropeller);
        login();

        ProfilePage profilePage = new ProfilePage(driver);
        profilePage.openProfile();
        profilePage.openPaymentInfo();
        profilePage.setCardNumber(cardNumber);
        profilePage.choosePaymentSystem(paymentSystem);
        profilePage.selectDayOfPayment();
        profilePage.clickSavePaymentInfo();
        Assert.assertTrue(profilePage.messagePleaseSetYourPaymentSystem().contains("Please select your payment system."), "Сообщение о не заполненном поле платежная система" +
                "не отобразилось");
    }

}

