package base;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Base {
    public String siteQaPropeller = "172.17.0.3/";
    public WebDriver driver;
    public WebDriverWait wait;

    @BeforeMethod(groups = {"fail", "pass"})
    public void start() throws MalformedURLException {

        //локальный запуск для отладки
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("start-maximized");
//        driver = new ChromeDriver(options);
//        //driver = new FirefoxDriver(options);
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
//        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);


        //selenoid
        String browser = BrowserType.CHROME;
        //String browser = BrowserType.FIREFOX;
        if (browser == BrowserType.CHROME) {

            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setBrowserName("chrome");
            capabilities.setVersion("78.0");
            capabilities.setCapability("enableVNC", true);
            capabilities.setCapability("enableVideo", false);

            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);



        }else if(browser == BrowserType.FIREFOX){
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setBrowserName("firefox");
            capabilities.setVersion("70.0");
            capabilities.setCapability("enableVNC", true);
            capabilities.setCapability("enableVideo", false);

            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        }
        wait = new WebDriverWait(driver, 10);

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().setSize(new Dimension(1920,1080));


    }

    @AfterMethod(groups = {"fail", "pass"})
    public void stop() {
        driver.quit();
    }

    public void openSite(String site) {
        driver.get("http://" + site);
    }

    //потом переделать
    protected void login() {
        WebElement startInputLogin = driver.findElement(By.xpath("//div[@class='card-body']//div[1]"));
        startInputLogin.click();
        WebElement email = driver.findElement(By.id("loginInput"));
        email.click();
        email.sendKeys("test");

        WebElement startInputPassword = driver.findElement(By.xpath("//div[@class='card-body']//div[2]"));
        startInputPassword.click();
        WebElement password = driver.findElement(By.id("passwordInput"));
        password.sendKeys("test");

        WebElement element = driver.findElement(By.xpath("//button[contains(text(),'Hover me faster!')]"));
        Actions action = new Actions(driver);
        action.moveToElement(element).build().perform();

        WebElement signIn = driver.findElement(By.xpath("//div[contains(@class,'card-footer bg-transparent')]//div//img"));
        signIn.click();

        Alert alert = (new WebDriverWait(driver, 10)).until(ExpectedConditions.alertIsPresent());
        alert.accept();

        Alert alert2 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.alertIsPresent());
        alert2.accept();
    }

}
