package main;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class MainPage {

    private WebDriver driver;

    MainPage(WebDriver driver) {
        this.driver = driver;
    }


    void art() {
        List<WebElement> webElements = driver.findElements(By.className("tree-main-node"));
        for (WebElement ele : webElements) {
            System.out.println(ele.getText());
        }
    }

    int countArt() {
        List<WebElement> webElements = driver.findElements(By.className("tree-main-node"));
        return webElements.size();
    }

    int countAdvertisers() {
        List<WebElement> webElements = driver.findElements(By.xpath("//button[contains(text(),'Advertisers')]/following-sibling::div/div"));
        return webElements.size();
    }

    int countPublishers() {
        List<WebElement> webElements = driver.findElements(By.xpath("//button[contains(text(),'Publishers')]/following-sibling::div/div"));
        return webElements.size();
    }

    int countTopLevelClients() {
        List<WebElement> webElements = driver.findElements(By.xpath("//button[contains(text(),'Top level clients')]/following-sibling::div/div"));
        return webElements.size();
    }

    void expandListAdvertisers() {
        driver.findElement(By.xpath("//button[contains(text(),'Advertisers')]")).click();
    }

    void clickTestAdvertiser() {
        driver.findElement(By.xpath("//button[contains(text(),'Test Advertiser')]")).click();
    }

    void clickAdidas() {
        driver.findElement(By.xpath("//button[contains(text(),'Adidas')]")).click();
    }

    void expandListPublishers() {
        driver.findElement(By.xpath("//button[contains(text(),'Publishers')]")).click();
    }

    void clickYoutube() {
        driver.findElement(By.xpath("//button[contains(text(),'Youtube')]")).click();
        //driver.findElement(By.xpath("//div[@class='tree-main-node'][2]//div[@class='sub-tree-element'][1]")).click();
    }

    void clickInstagram() {
//        WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Instagram')]")));
//        element.click();
        //driver.findElement(By.xpath("//div[@class='tree-main-node'][2]//div[@class='sub-tree-element'][2]")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Instagram')]")).click();
    }

    void expandTopLevelClients() {
        driver.findElement(By.xpath("//button[contains(text(),'Top level clients')]")).click();
    }

    void clickJonSnow() {
        driver.findElement(By.xpath("//button[contains(text(),'Jon Snow')]")).click();
    }

    void clickArturFleck() {
        driver.findElement(By.xpath("//button[contains(text(),'Artur Fleck')]")).click();
    }

    void clickTimCook() {
        driver.findElement(By.xpath("//button[contains(text(),'Tim Cook')]")).click();
    }

    void clickBugsBunny() {
        driver.findElement(By.xpath("//button[contains(text(),'Bugs Bunny')]")).click();
    }

    void clickSashaGrey() {
        driver.findElement(By.xpath("//button[contains(text(),'Sasha Grey')]")).click();
    }

    void clickYou() {
        driver.findElement(By.xpath("//button[contains(text(),'You')]")).click();
    }

    void clickLeonelMessi() {
        driver.findElement(By.xpath("//button[contains(text(),'Leonel Messi')]")).click();
    }

    void clickTonyStark() {
        driver.findElement(By.xpath("//button[contains(text(),'Tony Stark')]")).click();
    }

    void clickElonMusk() {
        driver.findElement(By.xpath("//button[contains(text(),'Elon Musk')]")).click();
    }

    void clickDarthVader() {
        driver.findElement(By.xpath("//button[contains(text(),'Darth Vader')]")).click();
    }

    String getTitle() {
        WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h5[@class='card-title']")));
        return element.getText();
    }

    boolean buttonMoveToSaved() {
        WebElement element = driver.findElement(By.xpath("//div[@id='dataCard']//div[@class='card-body']//button[contains(text(),'Move to saved')]"));
        return element.getAttribute("disabled").equals("true");
    }

    void scrollDownTextArea() {
        WebElement element = driver.findElement(By.xpath("//textarea[@class='form-control']"));
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();",element);
    }
}

