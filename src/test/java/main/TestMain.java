package main;

import base.Base;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestMain extends Base {


    @Test(description = "Количество разделов должно быть 3", groups = "pass")
    void countArticles() {
        int count = 3;
        openSite(siteQaPropeller);
        login();

        MainPage mainPage = new MainPage(driver);
        Assert.assertEquals(count, mainPage.countArt());
    }

    @Test(description = "Количество Advertisers должно быть 2", groups = "pass")
    void countAdvertisers() {
        int count = 2;
        openSite(siteQaPropeller);
        login();

        MainPage mainPage = new MainPage(driver);
        Assert.assertEquals(count, mainPage.countAdvertisers());
    }

    @Test(description = "Количество Publishers должно быть 2", groups = "pass")
    void countPublishers() {
        int count = 2;
        openSite(siteQaPropeller);
        login();

        MainPage mainPage = new MainPage(driver);
        Assert.assertEquals(count, mainPage.countPublishers());
    }

    @Test(description = "Количество TopLevelClients должно быть 10", groups = "pass")
    void countTopLevelClients() {
        int count = 10;
        openSite(siteQaPropeller);
        login();

        MainPage mainPage = new MainPage(driver);
        Assert.assertEquals(count, mainPage.countTopLevelClients());
    }

    @Test(description = "Проверка, что статьи в Advertisers отображаются корректно", groups = "pass")
    void checkAdvertiser() {
        openSite(siteQaPropeller);
        login();

        MainPage mainPage = new MainPage(driver);
        mainPage.expandListAdvertisers();
        mainPage.clickTestAdvertiser();
        Assert.assertTrue(mainPage.getTitle().contains("Test Advertiser"), "В разделе Advertiser не открылся Test Advertiser");
        mainPage.clickAdidas();
        Assert.assertTrue(mainPage.getTitle().contains("Adidas"), "В разделе Advertiser не открылся Adidas");
    }

    @Test(description = "Проверка, что статьи в Publishers отображаются корректно", groups = "pass")
    void checkPublishers() {
        openSite(siteQaPropeller);
        login();

        MainPage mainPage = new MainPage(driver);
        mainPage.expandListPublishers();
        mainPage.clickYoutube();
        Assert.assertTrue(mainPage.getTitle().contains("Youtube"), "В разделе Publishers не открылся Youtube");
        mainPage.clickInstagram();
        Assert.assertTrue(mainPage.getTitle().contains("Instagram"), "В разделе Publishers не открылся Instagram");
    }

    @Test(description = "Проверка, что статьи в Top level clients отображаются корректно", groups = "fail")
    void checkTopLevelClients() {
        openSite(siteQaPropeller);
        login();

        MainPage mainPage = new MainPage(driver);
        mainPage.expandTopLevelClients();
        mainPage.clickJonSnow();
        Assert.assertTrue(mainPage.getTitle().contains("Jon Snow"), "В разделе Top Level Clients не открылся Jon Snow");
        mainPage.clickArturFleck();
        Assert.assertTrue(mainPage.getTitle().contains("Instagram"), "В разделе op Level Clients не открылся Artur Fleck");
        mainPage.clickTimCook();
        Assert.assertTrue(mainPage.getTitle().contains("Tim Cook"), "В разделе Top Level Clients не открылся Tim Cook");
        mainPage.clickBugsBunny();
        Assert.assertTrue(mainPage.getTitle().contains("Bugs Bunny"), "В разделе op Level Clients не открылся Bugs Bunny");
        mainPage.clickSashaGrey();
        Assert.assertTrue(mainPage.getTitle().contains("Sasha Grey"), "В разделе Top Level Clients не открылся Sasha Grey");
        mainPage.clickYou();
        Assert.assertTrue(mainPage.getTitle().contains("You"), "В разделе op Level Clients не открылся You");
        mainPage.clickLeonelMessi();
        Assert.assertTrue(mainPage.getTitle().contains("Leonel Messi"), "В разделе Top Level Clients не открылся Leonel Messi");
        mainPage.clickTonyStark();
        Assert.assertTrue(mainPage.getTitle().contains("Tony Stark"), "В разделе op Level Clients не открылся Tony Stark");
        mainPage.clickElonMusk();
        Assert.assertTrue(mainPage.getTitle().contains("Elon Musk"), "В разделе op Level Clients не открылся Elon Musk");
        mainPage.clickDarthVader();
        Assert.assertTrue(mainPage.getTitle().contains("Darth Vader"), "В разделе op Level Clients не открылся Darth Vader");
    }


    @Test(description = "Недоступность кнопки Move to saved", groups = "pass")
    void disabledButtonMoveToSaved() {
        openSite(siteQaPropeller);
        login();

        MainPage mainPage = new MainPage(driver);
        mainPage.expandListPublishers();
        mainPage.clickYoutube();
        Assert.assertTrue(mainPage.buttonMoveToSaved(),"Кнопка доступна для нажатия без прочтения всего текста");

    }

    @Test(description = "Доступность кнопки Move to saved", groups = "pass")
    void enabledButtonMoveToSaved() {
        openSite(siteQaPropeller);
        login();

        MainPage mainPage = new MainPage(driver);
        mainPage.expandListPublishers();
        mainPage.clickYoutube();
        mainPage.scrollDownTextArea();
        Assert.assertFalse(mainPage.buttonMoveToSaved(),"Кнопка недоступна для нажатия без прочтения всего текста");
    }
}
