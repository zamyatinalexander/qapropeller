import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(
        features = "src/test/features",
        //glue = "java.test",
        tags = "@1",
        strict = true,
        snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class RunnerTest extends AbstractTestNGCucumberTests {
}